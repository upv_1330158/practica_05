package course.examples.practica_05;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

       //image.setImageResource(R.drawable.name+".png");
        image = (ImageView) findViewById(R.id.imageView1);

        if(name.contains("manzana" )){
            image.setImageResource(R.drawable.manzana);
        }
        else if(name.contains("uva" )){
            image.setImageResource(R.drawable.uva);

        }else if(name.contains("sandia" )){
            image.setImageResource(R.drawable.sandia);

        }else if(name.contains("pineapple" )){
            image.setImageResource(R.drawable.pineapple);

        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
