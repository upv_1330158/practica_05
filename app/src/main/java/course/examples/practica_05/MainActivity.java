package course.examples.practica_05;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {
    public final static String EXTRA_MESSAGE = " ";
    public static Intent intent = null ;

    Button button;
    Button button1;
    Button button2;
    Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerOnButton();
    }

    public void addListenerOnButton() {

        intent = new Intent(this, Main2Activity.class);

        button = (Button) findViewById(R.id.btnChangeImage);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //image.setImageResource(R.drawable.manzana);
                intent.putExtra(EXTRA_MESSAGE, "manzana");
                startActivity(intent);
            }
        });

        button1 = (Button) findViewById(R.id.btnChangeImage1);
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "uva");
                startActivity(intent);
            }
        });

        button2 = (Button) findViewById(R.id.btnChangeImage2);
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "sandia");
                startActivity(intent);
            }
        });

        button3 = (Button) findViewById(R.id.btnChangeImage3);
        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "pineapple");
                startActivity(intent);
            }
        });
    }


}
